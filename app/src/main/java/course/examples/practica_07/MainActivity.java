package course.examples.practica_07;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TabHost;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Resources resources = getResources();

        TabHost tabHost = (TabHost) findViewById(android.R.id.tabhost);
        tabHost.setup();

        TabHost.TabSpec section;

        section = tabHost.newTabSpec("tab1");
        section.setContent(R.id.tab1);
        section.setIndicator("Mensaje");
        tabHost.addTab(section);
        section = tabHost.newTabSpec("tab2");
        section.setContent(R.id.tab2);
        section.setIndicator("Llamada");
        tabHost.addTab(section);
        tabHost.setCurrentTab(0);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void llamada(View view) {
        EditText editText = (EditText) findViewById(R.id.tel);
        String tel = "tel:" + editText.getText().toString();
        try {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse(tel));
            startActivity(callIntent);
        } catch (ActivityNotFoundException activityException) {
            Log.e("dialing-example", "Call failed", activityException);
        } catch (SecurityException secEx) {
            Log.e("dialing-example", "Call failed", secEx);
        }
        editText.setText(null);
    }

    public void sms(View vw){
        EditText tel = (EditText)findViewById(R.id.numero);
        String telefono = "tel:" + tel.getText().toString();

        EditText mensaje = (EditText)findViewById(R.id.mensaje);
        String msj =mensaje.getText().toString();

        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(telefono,null,msj,null,null);
        Toast.makeText(getApplicationContext(), "Enviado",Toast.LENGTH_LONG).show();
        tel.setText(null);
        mensaje.setText(null);

    }
}
